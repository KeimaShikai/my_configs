# my_configs

## TODO

* Research [bluetuith](https://darkhz.github.io/bluetuith/) to replace blueman
* Research [LeD3F/dwm](https://github.com/LeD3F/dwm) for nice dwm
* Setup [dotbot](https://github.com/anishathalye/dotbot) for fast config/tools deployment
* Move all config repos into `keima_configs` namespace

## Useful commands and tools

* checkinstall - create deb package during build of smth with `make` to be able to remove it via `dpkg -r <package_name>` later
* iwctl - alternative for `wicd` (because `wicd` writen in python2 and currently depricated)
* amixer - useful to work with audio in scripts
* pacmd/pactl - useful to get info on audio devices and their parameters
* pavucontrol - tool to setup and test audio devices (with gui)
* blueman - bluetooth manager
* rambox - social media/messengers etc. aggregator (**install from official site's deb package**)
* unclutter - mouse cursor auto-hide
* feh - wallpaper manager (**it's better to use nitrogen instead**)

## Useful commands and tools 2.0

* [go](https://go.dev/dl/)
* [cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html) (rust)
* [alacritty](https://github.com/alacritty/alacritty/blob/master/INSTALL.md)
* picom - compositor for the terminal transparency (`apt-get install picom`)
* [zeliij](https://zellij.dev/documentation/installation.html) - terminal multiplexer written in Rust
* exa - ls analogue which can dysplay glyphs and files git status (`cargo install exa`)
* [fish](https://fishshell.com/) - analogue for zsh (`apt-get install fish`)
* [starship](https://starship.rs/) - nice cross-shell prompt
* dunst - dmenu-ish notification-daemon (`apt-get install dunst`)

## AstroNvim installation

The guide is in [KeimaShikai/astronvim_custom_config](https://gitlab.com/KeimaShikai/astronvim_custom_config) repo

## Useful tips and knowledges

**snapd** is a pure evil.

To autofinish suggested by fish command you can use `ctrl + f` or right arrow.  
You can use kill + `<tab>` to autocomplete the PID of the process.

To sync system time use **ntp**:

```bash
sudo apt-get install ntp
sudo systemctl restart ntp
sudo systemctl enable ntp
```

To build [forked zellij](https://gitlab.com/firolunis/zellij):

```bash
git checkout v0.36.0-patched
cargo install mandown
cargo x install ~/.local/bin/
```

## PS remote play for Linux

The guide to install ~thestr4ng3r/chiaki (from sourcehut) is [here](https://git.sr.ht/~thestr4ng3r/chiaki).  
With this you can use PS remote play on your laptop.  
Additionally you can find the script to get your psn account id in the **remote_play** directory.

## Useful tips (outdated)

To copy from urxvt + tmux - simply use `shift+mouse` to highlight the text (to copy) and then use the middle mouse button to paste.  
**Note** that copy by highlighting works across the whole dwm system! You can even past into tmux via `ctrl+q + p`.

Use `rehash` to update "index" of 'zsh' after installation of new utils.

Nice translater addon for Firefox - **TWP - Translate Web Page**.

## Usefull guides

* iwctl guides [1](https://wiki.debian.org/WiFi/HowToUse), [2](https://www.debugpoint.com/connect-wifi-terminal-linux/)
* [systemd-resolved guide](https://wiki.archlinux.org/title/Systemd-resolved)
* [vpn dns resolving issue](https://askubuntu.com/a/1036209)
* bluetooth setup issues - [1](https://stackoverflow.com/questions/34709583/bluetoothctl-set-passkey), [2](https://wiki.debian.org/iwlwifi)
