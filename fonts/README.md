Path to put: `/usr/share/fonts/truetype/` or `~/.local/share/fonts/`.

Useful commands:
```bash
# Update fonts cache
fc-cache -fv
# Check fonts
fc-cache -v /usr/share/fonts
```

Useful links:
* Download up-to-date NerdFonts [here](https://www.nerdfonts.com/font-downloads)
* Search for the particular glyph [here](https://www.nerdfonts.com/cheat-sheet)
