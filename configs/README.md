# Configs

* config.kdl - zellij config from `~/.config/zellij`
* config.fish - fish shell config from `~/.config/fish`
* starship.toml - starship cross shell prompt config from `~/.config`
* dunstrc - dunst config from `~/.config/dunst` (currently for v1.5)

# Outdated configs

* .tmux.conf
* .zshrc
* .zsh_aliases
