if status is-interactive
    # Commands to run in interactive sessions can go here
    set PATH $PATH ~/go/bin ~/.local/bin ~/.pulumi/bin ~/.config/emacs/bin

    # Setup node
    set --universal nvm_default_version v18.16.0

    # Aliases
    alias ls 'exa --icons -FHg --group-directories-first --git'

    # Tmp solution for dot shortcuts
    alias ... '../..'
    alias .... '../../..'
    alias ..... '../../../..'
    alias ...... '../../../../..'

    alias vpn_LAB 'sudo openvpn /etc/openvpn/configs/SecGen_danil_LAB.ovpn'
    alias vpn_MSK 'sudo openvpn /etc/openvpn/configs/SecGen_danil_MSK.ovpn'
    alias vpn_MSK-full 'sudo openvpn /etc/openvpn/configs/SecGen_danil_MSK-full.ovpn'
    alias vpn_MSK-tcp 'sudo openvpn /etc/openvpn/configs/SecGen_danil_MSK-tcp.ovpn'
    alias vpn_Telecom 'sudo openvpn /etc/openvpn/configs/SecGen_danil_Telecom.ovpn'
    alias vpn_SG 'sudo openvpn /etc/openvpn/configs/SG_danil_FSN.ovpn'
    alias vpn_SG-tcp 'sudo openvpn /etc/openvpn/configs/SG_danil_FSN-tcp.ovpn'

    # Turn off greeting message
    set fish_greeting

    # Setup starship
    starship init fish | source

    # Setup zellij
    set ZELLIJ_AUTO_ATTACH true
    set ZELLIJ_AUTO_EXIT true
    set ZELLIJ_SESSION_NAME ""

    # eval (zellij setup --generate-auto-start fish | string collect)

    if not set -q ZELLIJ
        if test "$ZELLIJ_AUTO_ATTACH" = "true"
            zellij attach -c "$ZELLIJ_SESSION_NAME"
        else
            zellij --session-name "$ZELLIJ_SESSION_NAME"
        end

        if test "$ZELLIJ_AUTO_EXIT" = "true"
            kill $fish_pid
        end
    # Workaround to create new tabs and panes in the home dir
    #else
        #cd
    end
end
